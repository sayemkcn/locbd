package net.toracode;

import net.toracode.domains.Location;
import net.toracode.service.DistrictService;
import net.toracode.service.DivisionService;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println(new DivisionService().findAll());
        System.out.println(new DistrictService().findById(40));
        System.out.println(new DivisionService().getAllChild(3, Location.Type.DISTRICT));
        System.out.println(new DistrictService().getParent("ফরিদপুর"));
    }
}
