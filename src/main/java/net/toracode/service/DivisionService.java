package net.toracode.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.toracode.domains.District;
import net.toracode.domains.Division;
import net.toracode.domains.Location;

import javax.xml.crypto.Data;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sayemkcn on 2/14/17.
 */
public class DivisionService extends LocationService {

    private List<Division> divisionList;

    public DivisionService() {
        this.divisionList = this.parseDivisionList(DataService.getJsonArray("https://gist.githubusercontent.com/sayemkcn/b54f84cd26e6a49a17b355d218611948/raw/b8ff351f20426cb6b72d10e2eb9b2bdab4d978e0/divisions.json"));
    }

    @Override
    public List<? extends Location> findAll() {
        return this.divisionList;
    }

    @Override
    public Location findById(int id) {
        if (this.divisionList != null)
            for (Division division : this.divisionList) {
                if (division.getId() == id) return division;
            }
        return null;
    }

    @Override
    public List<? extends Location> getAllChild(int id, Location.Type childType) {
        if (childType.equals(Location.Type.DISTRICT)) {
            List<District> newList = new ArrayList<>();
            List<District> districtList = (List<District>) new DistrictService().findAll();
            for (District district : districtList) {
                if (district.getDivisionId() == id)
                    newList.add(district);
            }
            return newList;
        }
        return null;
    }


    @Override
    public Location getParent(int childId) {
        return null;
    }

    @Override
    public Location getParent(String childName) {
        return null;
    }


    private List<Division> parseDivisionList(String jsonString) {
        Type listType = new TypeToken<List<Division>>() {
        }.getType();
        return (List<Division>) new Gson().fromJson(jsonString, listType);
    }
}
