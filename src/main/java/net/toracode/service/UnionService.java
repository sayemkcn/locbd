package net.toracode.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.toracode.domains.Location;
import net.toracode.domains.Union;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by sayemkcn on 2/15/17.
 */
public class UnionService extends LocationService {
    private List<Union> unionList;

    public UnionService() {
        this.unionList = this.parseUnionList(DataService.getJsonArray("https://gist.githubusercontent.com/sayemkcn/6d18b859a2eb0b7566824694b712b1b4/raw/87fb54614a3f9243e2873591a36cfd0c4fb0222b/unions.json"));
    }

    @Override
    public List<? extends Location> findAll() {
        return this.unionList;
    }

    @Override
    public Location findById(int id) {
        return null;
    }

    @Override
    public List<? extends Location> getAllChild(int parentId, Location.Type childType) {
        return null;
    }

    @Override
    public Location getParent(int childId) {
        return null;
    }

    @Override
    public Location getParent(String childName) {
        return null;
    }

    private List<Union> parseUnionList(String jsonString) {
        Type listType = new TypeToken<List<Union>>() {
        }.getType();
        return (List<Union>) new Gson().fromJson(jsonString, listType);
    }
}
