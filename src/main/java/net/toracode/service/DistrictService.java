package net.toracode.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.toracode.domains.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sayemkcn on 2/14/17.
 */
public class DistrictService extends LocationService {
    private List<District> districtList;

    public DistrictService() {
        this.districtList = this.parseDistrictList(DataService.getJsonArray("https://gist.githubusercontent.com/sayemkcn/f339a6e77928e92f7fbbf645f2cb78df/raw/c04b2994ff1e94a53ea1eb2459b83b5b75a8a615/districts.json"));
    }

    @Override
    public List<? extends Location> findAll() {
        return this.districtList;
    }

    @Override
    public Location findById(int id) {
        for (District district : this.districtList) {
            if (district.getId() == id)
                return district;
        }
        return null;
    }

    @Override
    public List<? extends Location> getAllChild(int parentId, Location.Type childType) {
        if (childType.equals(Location.Type.THANA)) {
            List<Thana> newList = new ArrayList<>();
            List<Thana> thanaList = (List<Thana>) new ThanaService().findAll();
            for (Thana thana: thanaList) {
                if (thana.getDistrictId() == parentId)
                    newList.add(thana);
            }
            return newList;
        }else if (childType.equals(Location.Type.UPAZILA)) {
            List<Upazila> newList = new ArrayList<>();
            List<Upazila> upazilaList = (List<Upazila>) new UpazilaService().findAll();
            for (Upazila upazila: upazilaList) {
                if (upazila.getDistrictId() == parentId)
                    newList.add(upazila);
            }
            return newList;
        }else if (childType.equals(Location.Type.MUNICIPALITY)) {
            List<Municipality> newList = new ArrayList<>();
            List<Municipality> municipalityList= (List<Municipality>) new MunicipalitySevice().findAll();
            for (Municipality municipality: municipalityList) {
                if (municipality.getDistrictId() == parentId)
                    newList.add(municipality);
            }
            return newList;
        }
        return null;
    }


    @Override
    public Location getParent(int childId) {
        List<District> districtList = (List<District>) this.findAll();
        for (District district : districtList) {
            if (childId == district.getId()) {
                return new DivisionService().findById(district.getDivisionId());
            }
        }
        return null;
    }

    @Override
    public Location getParent(String childName) {
        List<District> districtList = (List<District>) this.findAll();
        for (District district : districtList) {
            if (childName.toLowerCase().equals(district.getNameBn()) || childName.toLowerCase().equals(district.getNameEn().toLowerCase())) {
                return new DivisionService().findById(district.getDivisionId());
            }
        }
        return null;
    }


    private List<District> parseDistrictList(String jsonString) {
        Type listType = new TypeToken<List<District>>() {
        }.getType();
        return (List<District>) new Gson().fromJson(jsonString, listType);
    }
}
