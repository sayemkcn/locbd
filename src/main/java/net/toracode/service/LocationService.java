package net.toracode.service;

import net.toracode.domains.Location;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by sayemkcn on 2/14/17.
 */
public abstract class LocationService {

    public String readFile(String path) {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(path), StandardCharsets.UTF_8)) {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    abstract public List<? extends Location> findAll();
    abstract public Location findById(int id);
    abstract public List<? extends Location> getAllChild(int parentId, Location.Type childType);
    abstract public Location getParent(int childId);
    abstract public Location getParent(String childName);
}
