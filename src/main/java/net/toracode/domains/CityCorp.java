package net.toracode.domains;

/**
 * Created by sayemkcn on 2/14/17.
 */
public class CityCorp extends Location {
    private int divisionId;
    private int districtId;

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }


    @Override
    public String toString() {
        return "CityCorp{" +
                "divisionId=" + divisionId +
                ", districtId=" + districtId +
                "} " + super.toString();
    }
}
