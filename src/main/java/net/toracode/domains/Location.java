package net.toracode.domains;

/**
 * Created by sayemkcn on 2/14/17.
 */
public class Location {
    private int id;
    private String nameEn;
    private String nameBn;
    public static enum Type{DIVISION,DISTRICT,CITY_CORPORATION,MUNICIPALITY,THANA,UPAZILA,UNION}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameBn() {
        return nameBn;
    }

    public void setNameBn(String nameBn) {
        this.nameBn = nameBn;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", nameEn='" + nameEn + '\'' +
                ", nameBn='" + nameBn + '\'' +
                '}';
    }
}
