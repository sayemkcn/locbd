package net.toracode.domains;

/**
 * Created by sayemkcn on 2/14/17.
 */
public class District extends Location {
    private int divisionId;

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    @Override
    public String toString() {
        return "District{" +
                "divisionId=" + divisionId +
                "} " + super.toString();
    }
}
