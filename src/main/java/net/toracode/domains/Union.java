package net.toracode.domains;

/**
 * Created by sayemkcn on 2/14/17.
 */
public class Union extends Location {
    private int divisionId;
    private int districtId;
    private int upazilaId;

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getUpazilaId() {
        return upazilaId;
    }

    public void setUpazilaId(int upazilaId) {
        this.upazilaId = upazilaId;
    }

    @Override
    public String toString() {
        return "Union{" +
                "divisionId=" + divisionId +
                ", districtId=" + districtId +
                ", upazilaId=" + upazilaId +
                "} " + super.toString();
    }
}
