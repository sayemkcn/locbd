package net.toracode.repository;

import net.toracode.domains.Location;
import net.toracode.service.DivisionService;
import net.toracode.service.LocationService;

import java.util.List;

/**
 * Created by sayemkcn on 2/14/17.
 */
public class DivisionRepository implements LocationRepository {

    private LocationService locationService = new DivisionService();

    @Override
    public List<? extends Location> findAll() {
        return this.locationService.findAll();
    }

    @Override
    public Location getById(int id, Location.Type locationType) {
        return null;
    }

    @Override
    public List<Location> getAllChild(int id) {
        return null;
    }
}
