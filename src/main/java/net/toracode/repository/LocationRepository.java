package net.toracode.repository;

import net.toracode.domains.Location;

import java.util.List;

/**
 * Created by sayemkcn on 2/14/17.
 */
public interface LocationRepository {
    List<? extends Location> findAll();

    Location getById(int id, Location.Type locationType);

    List<Location> getAllChild(int id);
}
